﻿using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Repository
{
    public class SurveyRepository : BaseRepository<Survey, SurveyContext>
    {
        public SurveyRepository(SurveyContext context)
            : base(context)
        {
        }
                
    }
}