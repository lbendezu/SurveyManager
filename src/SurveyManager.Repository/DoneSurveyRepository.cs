﻿using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Repository
{
    public class DoneSurveyRepository: BaseRepository<DoneSurvey, SurveyContext>
    {
        public DoneSurveyRepository(SurveyContext context)
            : base(context)
        {
        }
                
    }
}
