﻿using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Repository
{
    public class SubjectRepository : BaseRepository<Subject, SurveyContext>
    {
        public SubjectRepository(SurveyContext context)
            : base(context)
        {
        }
                
    }
}