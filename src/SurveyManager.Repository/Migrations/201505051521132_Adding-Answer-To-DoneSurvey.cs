namespace SurveyManager.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingAnswerToDoneSurvey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DoneQuestions", "Score", c => c.Int(nullable: false));
            AddColumn("dbo.DoneSurveys", "Answer", c => c.String());
            DropColumn("dbo.DoneQuestions", "ScoreAnswer");
            DropColumn("dbo.DoneQuestions", "Answer");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DoneQuestions", "Answer", c => c.String());
            AddColumn("dbo.DoneQuestions", "ScoreAnswer", c => c.Int(nullable: false));
            DropColumn("dbo.DoneSurveys", "Answer");
            DropColumn("dbo.DoneQuestions", "Score");
        }
    }
}
