namespace SurveyManager.Repository.Migrations
{
    using SurveyManager.Domain;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SurveyManager.Repository.SurveyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SurveyManager.Repository.SurveyContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //

            var oTeacherGarcia = new Teacher { Id = 1, Name = "Luis Garcia" };

            var oInstitutionLima = new Institution { Id = 1, Name = "Universidad de Lima" };
            var oInstitutionUPC = new Institution { Id = 2, Name = "Universidad Peruana de Ciencias Aplicadas" };

            var oSubjectBasico = new Subject { Id = 1, Name = "Lenguaje de programaci�n b�sico" };
            var oSubjectIntermedio = new Subject { Id = 2, Name = "Lenguaje de programaci�n intermedio" };

            var oTISGarciaLimaBasico = new TeacherInstitutionSubject
            {
                Id = 1,
                Teacher = oTeacherGarcia,
                Institution = oInstitutionLima,
                Subject = oSubjectBasico
            };

            var oTISGarciaUPCBasico = new TeacherInstitutionSubject
                {
                    Id = 2,
                    Teacher = oTeacherGarcia,
                    Institution = oInstitutionUPC,
                    Subject = oSubjectBasico
                };

            var oTISGarciaLimaIntermedio = new TeacherInstitutionSubject
                {
                    Id = 3,
                    Teacher = oTeacherGarcia,
                    Institution = oInstitutionLima,
                    Subject = oSubjectIntermedio
                };

            context.TeacherInstitutionSubject.AddOrUpdate(oTISGarciaLimaBasico, oTISGarciaUPCBasico, oTISGarciaLimaIntermedio);

            context.Surveys.AddOrUpdate(
                new Survey
                {
                    Id = 1,
                    TeacherInstitutionSubject = oTISGarciaLimaBasico,
                    Name = "Encuesta de medio ciclo",
                    Description = "Esta encuesta fue dise�ada para medir el grado de satisfacci�n de los alumnos a la mitad del ciclo",
                    Questions = GetGenericQuestionsForSeed(1)
                },
                new Survey
                {
                    Id = 2,
                    TeacherInstitutionSubject = oTISGarciaLimaBasico,
                    Name = "Encuesta de fin ciclo",
                    Description = "Esta encuesta fue dise�ada para medir el grado de satisfacci�n de los alumnos al final del ciclo",
                    Questions = GetGenericQuestionsForSeed(2)
                },
                new Survey
                {
                    Id = 3,
                    TeacherInstitutionSubject = oTISGarciaUPCBasico,
                    Name = "Encuesta de medio ciclo",
                    Description = "Esta encuesta fue dise�ada para medir el grado de satisfacci�n de los alumnos a la mitad del ciclo",
                    Questions = GetGenericQuestionsForSeed(3)
                },
                new Survey
                {
                    Id = 4,
                    TeacherInstitutionSubject = oTISGarciaUPCBasico,
                    Name = "Encuesta de fin ciclo",
                    Description = "Esta encuesta fue dise�ada para medir el grado de satisfacci�n de los alumnos al final del ciclo",
                    Questions = GetGenericQuestionsForSeed(4)
                },
                new Survey
                {
                    Id = 5,
                    TeacherInstitutionSubject = oTISGarciaLimaIntermedio,
                    Name = "Encuesta de medio ciclo",
                    Description = "Esta encuesta fue dise�ada para medir el grado de satisfacci�n de los alumnos a la mitad del ciclo",
                    Questions = GetGenericQuestionsForSeed(5)
                },
                new Survey
                {
                    Id = 6,
                    TeacherInstitutionSubject = oTISGarciaLimaIntermedio,
                    Name = "Encuesta de fin ciclo",
                    Description = "Esta encuesta fue dise�ada para medir el grado de satisfacci�n de los alumnos al final del ciclo",
                    Questions = GetGenericQuestionsForSeed(6)
                });

        }

        private List<Question> GetGenericQuestionsForSeed(int IdSurvey)
        {
            return new List<Question>() { 
                        new Question{
                            Id = ((IdSurvey-1)*5) + 1,
                            IdSurvey = IdSurvey,
                            Description = "�Piensas que el profesor demostr� que conoc�a el tema?"
                        },
                        new Question{
                            Id = ((IdSurvey-1)*5) + 2,
                            IdSurvey = IdSurvey,
                            Description = "�El profesor demostr� inter�s en que los alumnos aprendan?"
                        },
                        new Question{
                            Id = ((IdSurvey-1)*5) + 3,
                            IdSurvey = IdSurvey,
                            Description = "�El profesor fue siempre puntual?"
                        },
                        new Question{
                            Id = ((IdSurvey-1)*5) + 4,
                            IdSurvey = IdSurvey,
                            Description = "�El profesor utiliz� una metodolog�a de ense�anza efectiva?"
                        },
                        new Question{
                            Id = ((IdSurvey-1)*5) + 5,
                            IdSurvey = IdSurvey,
                            Description = "�El profesor trat� a los alumnos de manera respetuosa en todo momento?"
                        }
                    };
        }
    }
}
