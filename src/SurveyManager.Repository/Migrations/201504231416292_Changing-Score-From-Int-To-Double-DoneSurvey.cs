namespace SurveyManager.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangingScoreFromIntToDoubleDoneSurvey : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DoneSurveys", "Score", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DoneSurveys", "Score", c => c.Int(nullable: false));
        }
    }
}
