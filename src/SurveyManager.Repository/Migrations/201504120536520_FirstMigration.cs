namespace SurveyManager.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DoneQuestions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Answer = c.String(),
                        IdQuestion = c.Int(nullable: false),
                        IdDoneSurvey = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DoneSurveys", t => t.IdDoneSurvey)
                .ForeignKey("dbo.Questions", t => t.IdQuestion)
                .Index(t => t.IdQuestion)
                .Index(t => t.IdDoneSurvey);
            
            CreateTable(
                "dbo.DoneSurveys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Score = c.Int(nullable: false),
                        SurveyDate = c.DateTime(nullable: false),
                        IdSurvey = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Surveys", t => t.IdSurvey)
                .Index(t => t.IdSurvey);
            
            CreateTable(
                "dbo.Surveys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        IdTeacherInstitutionSubject = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TeacherInstitutionSubjects", t => t.IdTeacherInstitutionSubject)
                .Index(t => t.IdTeacherInstitutionSubject);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        IdSurvey = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Surveys", t => t.IdSurvey)
                .Index(t => t.IdSurvey);
            
            CreateTable(
                "dbo.TeacherInstitutionSubjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdTeacher = c.Int(nullable: false),
                        IdInstitution = c.Int(nullable: false),
                        IdSubject = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Institutions", t => t.IdInstitution)
                .ForeignKey("dbo.Subjects", t => t.IdSubject)
                .ForeignKey("dbo.Teachers", t => t.IdTeacher)
                .Index(t => t.IdTeacher)
                .Index(t => t.IdInstitution)
                .Index(t => t.IdSubject);
            
            CreateTable(
                "dbo.Institutions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DoneQuestions", "IdQuestion", "dbo.Questions");
            DropForeignKey("dbo.DoneSurveys", "IdSurvey", "dbo.Surveys");
            DropForeignKey("dbo.Surveys", "IdTeacherInstitutionSubject", "dbo.TeacherInstitutionSubjects");
            DropForeignKey("dbo.TeacherInstitutionSubjects", "IdTeacher", "dbo.Teachers");
            DropForeignKey("dbo.TeacherInstitutionSubjects", "IdSubject", "dbo.Subjects");
            DropForeignKey("dbo.TeacherInstitutionSubjects", "IdInstitution", "dbo.Institutions");
            DropForeignKey("dbo.Questions", "IdSurvey", "dbo.Surveys");
            DropForeignKey("dbo.DoneQuestions", "IdDoneSurvey", "dbo.DoneSurveys");
            DropIndex("dbo.TeacherInstitutionSubjects", new[] { "IdSubject" });
            DropIndex("dbo.TeacherInstitutionSubjects", new[] { "IdInstitution" });
            DropIndex("dbo.TeacherInstitutionSubjects", new[] { "IdTeacher" });
            DropIndex("dbo.Questions", new[] { "IdSurvey" });
            DropIndex("dbo.Surveys", new[] { "IdTeacherInstitutionSubject" });
            DropIndex("dbo.DoneSurveys", new[] { "IdSurvey" });
            DropIndex("dbo.DoneQuestions", new[] { "IdDoneSurvey" });
            DropIndex("dbo.DoneQuestions", new[] { "IdQuestion" });
            DropTable("dbo.Teachers");
            DropTable("dbo.Subjects");
            DropTable("dbo.Institutions");
            DropTable("dbo.TeacherInstitutionSubjects");
            DropTable("dbo.Questions");
            DropTable("dbo.Surveys");
            DropTable("dbo.DoneSurveys");
            DropTable("dbo.DoneQuestions");
        }
    }
}
