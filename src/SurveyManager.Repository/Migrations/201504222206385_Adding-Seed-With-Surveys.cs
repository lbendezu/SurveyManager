namespace SurveyManager.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingSeedWithSurveys : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DoneQuestions", "ScoreAnswer", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DoneQuestions", "ScoreAnswer");
        }
    }
}
