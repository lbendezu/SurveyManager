﻿using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Repository
{

    public class InstitutionRepository : BaseRepository<Institution, SurveyContext>
    {
        public InstitutionRepository(SurveyContext context)
            : base(context)
        {
        }
                
    }
}