﻿using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Repository
{

    public class TeacherRepository : BaseRepository<Teacher, SurveyContext>
    {
        public TeacherRepository(SurveyContext context)
            : base(context)
        {
        }
                
    }
}