﻿using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Repository
{
    public interface IRepository<TEntity> : IDisposable
        where TEntity:EntityBase
    {
        IQueryable<TEntity> Get();
        int Create(TEntity entity);
        int Update(TEntity entity);
        int Delete(TEntity entity);
    }
}