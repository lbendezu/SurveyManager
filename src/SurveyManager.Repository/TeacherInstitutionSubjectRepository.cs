﻿using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Repository
{
    public class TeacherInstitutionSubjectRepository: BaseRepository<TeacherInstitutionSubject, SurveyContext>
    {
        public TeacherInstitutionSubjectRepository(SurveyContext context)
            : base(context)
        {
        }
                
    }
}
