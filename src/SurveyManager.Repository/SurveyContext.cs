﻿using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Repository
{
    public class SurveyContext : DbContext
    {
        public SurveyContext()
            : base("name=SurveyConnection")
        {
        }

        public DbSet<DoneQuestion> DoneQuestions { get; set; }
        public DbSet<DoneSurvey> DoneSurveys { get; set; }
        public DbSet<Institution> Institutions { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<TeacherInstitutionSubject> TeacherInstitutionSubject { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
