﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Common
{
    public class DoneQuestionViewModel
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public DateTime SurveyDate { get; set; }
        public int IdDoneSurvey { get; set; }
        public int IdQuestion { get; set; }
    }
}
