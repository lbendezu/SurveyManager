﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Common
{
    public class SurveyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IdInstitution { get; set; }
        public int IdTeacher { get; set; }
        public int IdSubject { get; set; }
        public string InstitutionName { get; set; }
        public string TeacherName { get; set; }
        public string SubjectName { get; set; }
        public List<QuestionViewModel> Questions { get; set; }
        public double Average { get; set; }
    }
}
