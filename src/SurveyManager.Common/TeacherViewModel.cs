﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Common
{
    public class TeacherViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
