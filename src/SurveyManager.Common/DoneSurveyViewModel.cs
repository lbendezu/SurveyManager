﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Common
{
    public class DoneSurveyViewModel
    {
        public int Id { get; set; }
        public string Answer { get; set; }
        public double Score { get; set; }
        public DateTime SurveyDate { get; set; }
        public int IdSurvey { get; set; }
        public List<DoneQuestionViewModel> Questions { get; set; }
    }
}
