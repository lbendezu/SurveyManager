﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Domain
{
    public class Question : EntityBase
    {
        public string Description { get; set; }        

        public int IdSurvey { get; set; }
        [ForeignKey("IdSurvey")]
        public virtual Survey Survey { get; set; }
    }
}
