﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Domain
{
    public class DoneQuestion : EntityBase
    {
        public int Score { get; set; }
        
        public int IdQuestion { get; set; }
        [ForeignKey("IdQuestion")]
        public virtual Question Question { get; set; }

        public int IdDoneSurvey { get; set; }
        [ForeignKey("IdDoneSurvey")]
        public virtual DoneSurvey DoneSurvey { get; set; }
    }
}
