﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Domain
{
    public class TeacherInstitutionSubject : EntityBase
    {
        public int IdTeacher { get; set; }
        [ForeignKey("IdTeacher")]
        public virtual Teacher Teacher { get; set; }

        public int IdInstitution { get; set; }
        [ForeignKey("IdInstitution")]
        public virtual Institution Institution { get; set; }

        public int IdSubject { get; set; }
        [ForeignKey("IdSubject")]
        public virtual Subject Subject { get; set; }
    }
}
