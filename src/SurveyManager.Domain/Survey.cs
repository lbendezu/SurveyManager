﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Domain
{
    public class Survey : EntityBase
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int IdTeacherInstitutionSubject { get; set; }
        [ForeignKey("IdTeacherInstitutionSubject")]        
        public virtual TeacherInstitutionSubject TeacherInstitutionSubject { get; set; }
        
        public virtual ICollection<Question> Questions { get; set; }
    }
}
