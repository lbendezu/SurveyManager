﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Domain
{
    public class DoneSurvey : EntityBase
    {
        public double Score { get; set; }

        public string Answer { get; set; }

        public DateTime SurveyDate { get; set; }

        public int IdSurvey { get; set; }
        [ForeignKey("IdSurvey")]
        public virtual Survey Survey { get; set; }

        public virtual ICollection<DoneQuestion> DoneQuestions { get; set; }
    }
}
