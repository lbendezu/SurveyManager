﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using SurveyManager.Domain;
using SurveyManager.Repository;
using SurveyManager.Services;
using SurveyManager.Common;
using NUnit.Framework;
using Rhino.Mocks;
using List = Rhino.Mocks.Constraints.List;

namespace SurveyManager.Tests.Services
{
    [TestFixture]
    public class SurveyServiceTests
    {
        private IRepository<Survey> surveyRepositoryStub;
        private IRepository<TeacherInstitutionSubject> teacherInstitutionSubjectRepositoryStub;
        private IRepository<DoneSurvey> doneSurveyRepositoryStub;
        private SurveyService surveyService;

        [SetUp]
        public void Setup()
        {
            surveyRepositoryStub = MockRepository.GenerateMock<IRepository<Survey>>();
            teacherInstitutionSubjectRepositoryStub = MockRepository.GenerateMock<IRepository<TeacherInstitutionSubject>>();
            doneSurveyRepositoryStub = MockRepository.GenerateMock<IRepository<DoneSurvey>>();
            surveyService = new SurveyService(surveyRepositoryStub, teacherInstitutionSubjectRepositoryStub, doneSurveyRepositoryStub);
        }

        [TearDown]
        public void TearDown()
        {
            surveyRepositoryStub = null;
            teacherInstitutionSubjectRepositoryStub = null;
            doneSurveyRepositoryStub = null;
            surveyService = null;
        }

        #region GetAllSurveys

        [Test]
        public void GetAllSurveys_DadoQueExistenEncuestas_DevuelveLasEncuestasEncontradas()
        {
            surveyRepositoryStub.Stub(m => m.Get())
                .Return(
                new EnumerableQuery<Survey>(new List<Survey>() { new Survey() { Id = 1, Name = "Encuesta 1" },
                                                                 new Survey() { Id = 2, Name = "Encuesta 2" },
                                                                 new Survey() { Id = 2, Name = "Encuesta 3" }}));

            var surveys = surveyService.GetAllSurveys(); //act

            surveys.Should().NotBeEmpty("No deberia ser una lista vacia");
            surveys.Count().Should().Be(3, "Debería devolver 3 elementos");

        }

        [Test]
        public void GetAllSurveys_NoExistenEncuestas_DevuelveUnaListaVacia()
        {
            surveyRepositoryStub.Stub(m => m.Get())
                .Return(
                new EnumerableQuery<Survey>(new List<Survey>()));

            var surveys = surveyService.GetAllSurveys(); //act
            surveys.Should().BeEmpty("Deberia ser una lista vacia");

        }

        #endregion


        #region GetAllSurveyByAllIds

        [Test]
        public void GetAllSurveyByAllIds_DadoQueExistenDatosPorInstitucionProfesorYCurso_DevuelveListaDeEncuestas()
        {
            teacherInstitutionSubjectRepositoryStub.Stub(m => m.Get())
               .Return(
                new EnumerableQuery<TeacherInstitutionSubject>(new List<TeacherInstitutionSubject>() { new TeacherInstitutionSubject() { Id = 1, IdInstitution = 1, IdTeacher = 1, IdSubject = 1 } }));

            surveyRepositoryStub.Stub(m => m.Get())
                .Return(
                new EnumerableQuery<Survey>(new List<Survey>()
                {
                    new Survey() { Id = 1, IdTeacherInstitutionSubject = 1},
                    new Survey() { Id = 2, IdTeacherInstitutionSubject = 1},
                    new Survey() { Id = 3, IdTeacherInstitutionSubject = 1}
                }));

            doneSurveyRepositoryStub.Stub(m => m.Get())
                .Return(new List<DoneSurvey>()
                {
                    new DoneSurvey() { Id = 1, IdSurvey = 1},
                    new DoneSurvey() { Id = 2, IdSurvey = 2},
                    new DoneSurvey() { Id = 3, IdSurvey = 3}
                }.AsQueryable());

            var surveys = surveyService.GetAllSurveyByAllIds(1, 1, 1); //act
            surveys.Should().NotBeEmpty("No deberia ser una lista vacia"); //fluent assertions
            surveys.Count().Should().Be(3, "Deberian ser 3");
        }

        [Test]
        public void GetAllSurveyByAllIds_DadoQueExistenDatosPorInstitucionProfesorYCurso_DevuelveListaDeEncuestasConPromediosCorrectos()
        {
            teacherInstitutionSubjectRepositoryStub.Stub(m => m.Get())
               .Return(
                new EnumerableQuery<TeacherInstitutionSubject>(new List<TeacherInstitutionSubject>() { new TeacherInstitutionSubject() { Id = 1, IdInstitution = 1, IdTeacher = 1, IdSubject = 1 } }));

            surveyRepositoryStub.Stub(m => m.Get())
                .Return(
                new EnumerableQuery<Survey>(new List<Survey>()
                {
                    new Survey() { Id = 1, IdTeacherInstitutionSubject = 1},
                    new Survey() { Id = 2, IdTeacherInstitutionSubject = 1},
                    new Survey() { Id = 3, IdTeacherInstitutionSubject = 1}
                }));

            doneSurveyRepositoryStub.Stub(m => m.Get())
                .Return(new List<DoneSurvey>()
                {
                    new DoneSurvey() { Id = 1, IdSurvey = 1, Score = 3.5},
                    new DoneSurvey() { Id = 2, IdSurvey = 1, Score = 2.6},
                    new DoneSurvey() { Id = 3, IdSurvey = 2, Score = 1.7},
                    new DoneSurvey() { Id = 4, IdSurvey = 2, Score = 3.5},
                    new DoneSurvey() { Id = 5, IdSurvey = 3, Score = 2.6},
                    new DoneSurvey() { Id = 6, IdSurvey = 3, Score = 1.7}
                }.AsQueryable());

            var surveys = surveyService.GetAllSurveyByAllIds(1, 1, 1); //act
            surveys.Should().NotBeEmpty("No deberia ser una lista vacia"); //fluent assertions
            surveys.ElementAt(0).Average.Should().Be((3.5 + 2.6) / 2, "Deberia ser (3.5 + 2.6) / 2");
            surveys.ElementAt(1).Average.Should().Be((1.7 + 3.5) / 2, "Deberia ser (1.7 + 3.5) / 2");
            surveys.ElementAt(2).Average.Should().Be((2.6 + 1.7) / 2, "Deberia ser (2.6 + 1.7) / 2");
            surveys.Count().Should().Be(3, "Deberian ser 3");
        }

        #endregion
    }
}


