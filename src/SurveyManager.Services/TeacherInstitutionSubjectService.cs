﻿using SurveyManager.Domain;
using SurveyManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Services
{
    public class TeacherInstitutionSubjectService: IDisposable
    {
        private IRepository<TeacherInstitutionSubject> _teacherInstitutionSubjectRepository;

        public TeacherInstitutionSubjectService(IRepository<TeacherInstitutionSubject> teacherInstitutionSubjectRepository)
        {
            _teacherInstitutionSubjectRepository = teacherInstitutionSubjectRepository;
        }

        public void Dispose()
        {
            _teacherInstitutionSubjectRepository = null;
        }

        public IEnumerable<Institution> GetInstitutions()
        {
            return _teacherInstitutionSubjectRepository.Get().Select(x => x.Institution).Distinct().OrderBy(x => x.Name).ToList();
        }

        public IEnumerable<Teacher> GetTeachersByInstitution(int IdInstitution)
        {
            return _teacherInstitutionSubjectRepository.Get().
                Where(x => x.IdInstitution == IdInstitution).Select(x => x.Teacher).Distinct().OrderBy(x => x.Name).ToList();
        }

        public IEnumerable<Subject> GetSubjectsByInstitutionAndTeacher(int IdInstitution, int IdTeacher)
        {
            return _teacherInstitutionSubjectRepository.Get()
                .Where(x => x.IdInstitution == IdInstitution && x.IdTeacher == IdTeacher).Select(x => x.Subject).Distinct().OrderBy(x => x.Name).ToList();
        }

    }
}