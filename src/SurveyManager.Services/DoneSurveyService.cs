﻿using SurveyManager.Common;
using SurveyManager.Domain;
using SurveyManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Services
{
    public class DoneSurveyService : IDisposable
    {
        private IRepository<DoneSurvey> _doneSurveyRepository;

        public DoneSurveyService(IRepository<DoneSurvey> doneSurveyRepository)
        {
            _doneSurveyRepository = doneSurveyRepository;
        }

        public void Dispose()
        {
            _doneSurveyRepository = null;
        }


        public int AssessSurvey(DoneSurveyViewModel doneSurveyVM)
        {
            var newDoneSurvey = new DoneSurvey();
            newDoneSurvey.IdSurvey = doneSurveyVM.IdSurvey;
            newDoneSurvey.SurveyDate = DateTime.Now;
            newDoneSurvey.DoneQuestions = new List<DoneQuestion>();
            newDoneSurvey.Answer = doneSurveyVM.Answer;

            double averageScore = 0.0;

            foreach (var question in doneSurveyVM.Questions)
            {
                averageScore += question.Score;
                newDoneSurvey.DoneQuestions.Add(new DoneQuestion() { Score = question.Score, IdQuestion = question.IdQuestion });
            }

            newDoneSurvey.Score = averageScore / newDoneSurvey.DoneQuestions.Count;
            
            return _doneSurveyRepository.Create(newDoneSurvey);
        }
    }
}
