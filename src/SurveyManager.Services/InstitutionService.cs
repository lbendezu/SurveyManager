﻿using SurveyManager.Common;
using SurveyManager.Domain;
using SurveyManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Services
{
    public class InstitutionService: IDisposable
    {
        private IRepository<Institution> _institutionRepository;

        public InstitutionService(IRepository<Institution> teacherRepository)
        {
            _institutionRepository = teacherRepository;
        }

        public void Dispose()
        {
            _institutionRepository = null;
        }

        public IEnumerable<Institution> GetAllInstitutions()
        {
            return _institutionRepository.Get().OrderBy(x => x.Name).ToList();
        }

        public int CreateInstitution(InstitutionViewModel institutionVM)
        {
            var newInstitution = new Institution();

            newInstitution.Name = institutionVM.Name;

            return _institutionRepository.Create(newInstitution);
        }
    }
}
