﻿using AutoMapper;
using SurveyManager.Common;
using SurveyManager.Domain;
using SurveyManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Services
{
    public class SurveyService : IDisposable
    {
        private IRepository<Survey> _surveyRepository;
        private IRepository<TeacherInstitutionSubject> _teacherInstitutionSubjectRepository;
        private IRepository<DoneSurvey> _doneSurveyRepository;

        public SurveyService(IRepository<Survey> surveyRepository, IRepository<TeacherInstitutionSubject> teacherInstitutionSubjectRepósitory, IRepository<DoneSurvey> doneSurveyRepository)
        {
            _surveyRepository = surveyRepository;
            _teacherInstitutionSubjectRepository = teacherInstitutionSubjectRepósitory;
            _doneSurveyRepository = doneSurveyRepository;
        }

        public void Dispose()
        {
            _surveyRepository = null;
            _teacherInstitutionSubjectRepository = null;
            _doneSurveyRepository = null;
        }

        public IEnumerable<Survey> GetAllSurveys()
        {
            return _surveyRepository.Get().OrderBy(x => x.Id).ToList();
        }

        public SurveyViewModel GetSurvey(int id)
        {
            var survey = _surveyRepository.Get().Where(x => x.Id == id).OrderBy(x => x.Id).SingleOrDefault();

            return Mapper.Map<SurveyViewModel>(survey);
        }

        public IEnumerable<SurveyViewModel> GetAllSurveyByAllIds(int IdInstitution, int IdTeacher, int IdSubject)
        {

            var teacherInstitutionSubject = _teacherInstitutionSubjectRepository.Get().Where(x =>
                                                 x.IdInstitution == IdInstitution &&
                                                 x.IdTeacher == IdTeacher &&
                                                 x.IdSubject == IdSubject).SingleOrDefault();

            if (teacherInstitutionSubject != null)
            {
                var surveys = _surveyRepository.Get().Where(x => x.IdTeacherInstitutionSubject == teacherInstitutionSubject.Id)
                    .Select(y => new SurveyViewModel
                    {
                        Id = y.Id,
                        Name = y.Name,
                        Description = y.Description,
                        Average = 0.0
                    })
                    .OrderBy(x => x.Name).ToList();

                foreach(var survey in surveys)
                {
                    // has done surveys?
                    var doneSurveys = _doneSurveyRepository.Get().Where(ds => ds.IdSurvey == survey.Id);
                    if (doneSurveys != null && doneSurveys.Count() > 0)
                    {
                        survey.Average = doneSurveys.Average(ds => ds.Score);
                    }
                }

                return surveys;
            }
            else 
            {
                return new List<SurveyViewModel>();
            }            
        }

        public int CreateSurvey(SurveyViewModel surveyVM)
        {
            var teacherInstitutionSubject = _teacherInstitutionSubjectRepository.Get().Where(x =>
                                                 x.IdInstitution == surveyVM.IdInstitution &&
                                                 x.IdTeacher == surveyVM.IdTeacher &&
                                                 x.IdSubject == surveyVM.IdSubject).SingleOrDefault();

            if (teacherInstitutionSubject == null)
            {
                teacherInstitutionSubject = new TeacherInstitutionSubject();
                teacherInstitutionSubject.IdTeacher = surveyVM.IdTeacher;
                teacherInstitutionSubject.IdInstitution = surveyVM.IdInstitution;
                teacherInstitutionSubject.IdSubject = surveyVM.IdSubject;

                _teacherInstitutionSubjectRepository.Create(teacherInstitutionSubject);

                teacherInstitutionSubject = _teacherInstitutionSubjectRepository.Get().Where(x =>
                                                 x.IdInstitution == surveyVM.IdInstitution &&
                                                 x.IdTeacher == surveyVM.IdTeacher &&
                                                 x.IdSubject == surveyVM.IdSubject).SingleOrDefault();
            }                

            var newSurvey = new Survey();

            newSurvey.Name = surveyVM.Name;
            newSurvey.Description = surveyVM.Description;
            newSurvey.IdTeacherInstitutionSubject = teacherInstitutionSubject.Id;
            newSurvey.Questions = new List<Question>();

            foreach (var question in surveyVM.Questions)
            {
                newSurvey.Questions.Add(new Question() { Description = question.Description });
            }

            return _surveyRepository.Create(newSurvey);
        }
    }
}
