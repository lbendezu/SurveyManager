﻿using SurveyManager.Common;
using SurveyManager.Domain;
using SurveyManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Services
{
    public class SubjectService: IDisposable
    {
        private IRepository<Subject> _subjectRepository;

        public SubjectService(IRepository<Subject> subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public void Dispose()
        {
            _subjectRepository = null;
        }

        public IEnumerable<Subject> GetAllSubjects()
        {
            return _subjectRepository.Get().OrderBy(x => x.Name).ToList();
        }

        public int CreateSubject(SubjectViewModel subjectVM)
        {
            var newSubject = new Subject();

            newSubject.Name = subjectVM.Name;

            return _subjectRepository.Create(newSubject);
        }
    }
}
