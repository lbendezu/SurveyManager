﻿using SurveyManager.Common;
using SurveyManager.Domain;
using SurveyManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurveyManager.Services
{
    public class TeacherService : IDisposable
    {
        private IRepository<Teacher> _teacherRepository;

        public TeacherService(IRepository<Teacher> teacherRepository)
        {
            _teacherRepository = teacherRepository;
        }

        public void Dispose()
        {
            _teacherRepository = null;
        }

        public IEnumerable<Teacher> GetAllTeachers()
        {
            return _teacherRepository.Get().OrderBy(x => x.Name).ToList();
        }

        public object GetTeachersByInstitution()
        {
            throw new NotImplementedException();
        }

        public int CreateTeacher(TeacherViewModel teacherVM)
        {
            var newTeacher = new Teacher();

            newTeacher.Name = teacherVM.Name;

            return _teacherRepository.Create(newTeacher);
        }
    }
}
