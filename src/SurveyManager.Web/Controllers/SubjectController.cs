﻿using SurveyManager.Common;
using SurveyManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveyManager.Web.Controllers
{
    public class SubjectController : Controller
    {
        private SubjectService _subjectService;

        public SubjectController(SubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        // GET: Subject
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllSubjects() {

            var subjects = _subjectService.GetAllSubjects();

            return Json(subjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(SubjectViewModel subject)
        {
            var subjects = _subjectService.CreateSubject(subject);

            return Json(subjects);
        }
    }
}