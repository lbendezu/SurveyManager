﻿using SurveyManager.Common;
using SurveyManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveyManager.Web.Controllers
{
    public class InstitutionController : Controller
    {
        private InstitutionService _institutionService;

        public InstitutionController(InstitutionService institutionService)
        {
            _institutionService = institutionService;
        }

        // GET: Institution
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllInstitutions() {

            var institutions = _institutionService.GetAllInstitutions();

            return Json(institutions, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(InstitutionViewModel institution)
        {
            var institutions = _institutionService.CreateInstitution(institution);

            return Json(institutions);
        }
    }
}