﻿using SurveyManager.Common;
using SurveyManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveyManager.Web.Controllers
{
    public class TeacherController : Controller
    {
        private TeacherService _teacherService;

        public TeacherController(TeacherService teacherService)
        {
            _teacherService = teacherService;
        }

        // GET: Teacher
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllTeachers() {

            var teachers = _teacherService.GetAllTeachers();

            return Json(teachers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(TeacherViewModel teacher)
        {
            var teachers = _teacherService.CreateTeacher(teacher);

            return Json(teachers);
        }
    }
}