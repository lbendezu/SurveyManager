﻿using SurveyManager.Common;
using SurveyManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveyManager.Web.Controllers
{
    public class SurveyController : Controller
    {
        private SurveyService _surveyService;
        private DoneSurveyService _doneSurveyService;

        public SurveyController(SurveyService surveyService, DoneSurveyService doneSurveyService)
        {
            _surveyService = surveyService;
            _doneSurveyService = doneSurveyService;
        }
        
        // GET: Survey
        public ActionResult Index()
        {
            var surveys = _surveyService.GetAllSurveys();

            return View(surveys);
        }

        [HttpGet]
        public JsonResult GetSurvey(int id)
        {
            var survey = _surveyService.GetSurvey(id);
            return Json(survey,JsonRequestBehavior.AllowGet);
        }

        
        [HttpGet]
        public JsonResult GetAllSurveyByAllIds(int IdInstitution, int IdTeacher, int IdSubject)
        {

            var surveys = _surveyService.GetAllSurveyByAllIds(IdInstitution, IdTeacher, IdSubject);

            return Json(surveys,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(SurveyViewModel survey)
        {
            var surveys = _surveyService.CreateSurvey(survey);

            return Json(surveys);
        }

        [HttpPost]
        public JsonResult Assess(DoneSurveyViewModel survey)
        {
            var surveys = _doneSurveyService.AssessSurvey(survey);

            return Json(surveys);
        }

    }
}