﻿using SurveyManager.Domain;
using SurveyManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SurveyManager.Web.Controllers
{
    public class TeacherInstitutionSubjectController  : Controller
    {
        private TeacherInstitutionSubjectService _teacherInstitutionSubjectService;

        public TeacherInstitutionSubjectController(TeacherInstitutionSubjectService teacherInstitutionSubjectService)
        {
            _teacherInstitutionSubjectService = teacherInstitutionSubjectService;
        }

        // GET: Teacher
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetInstitutions()
        {

            var institutions = _teacherInstitutionSubjectService.GetInstitutions();

            return Json(institutions, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetTeachersByInstitution(int IdInstitution)
        {

            var teachers = _teacherInstitutionSubjectService.GetTeachersByInstitution(IdInstitution);

            return Json(teachers, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubjectsByInstitutionAndTeacher(int IdInstitution, int IdTeacher)
        {

            var subjects = _teacherInstitutionSubjectService.GetSubjectsByInstitutionAndTeacher(IdInstitution, IdTeacher);

            return Json(subjects, JsonRequestBehavior.AllowGet);
        }

    }
}