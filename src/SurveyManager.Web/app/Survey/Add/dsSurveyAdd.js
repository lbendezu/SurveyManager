﻿
angularFormsApp.factory('dsSurveyAdd',
    ["$http",
    function ($http) {

        var getAllSurveyByAllIds = function (idInstitution, idTeacher, idSubject) {

            return $http.get("Survey/GetAllSurveyByAllIds", { params: { IdInstitution: idInstitution, IdTeacher: idTeacher, IdSubject: idSubject } });
        };

        var createSurvey = function (survey) {

            return $http.post("Survey/Create", survey);
        };


        return {
            getAllSurveyByAllIds: getAllSurveyByAllIds,
            createSurvey: createSurvey
        };
    }]);