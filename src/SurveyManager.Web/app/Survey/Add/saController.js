﻿angularFormsApp.controller('saController',
    ["$scope", "$window", "$routeParams", "dsInstitutions", "dsTeachers", "dsSubjects", "dsSurveyAdd",
    function efController($scope, $window, $routeParams, dsInstitutions, dsTeachers, dsSubjects, dsSurveyAdd) {

        /* --------------------------------------------------------------------- Begin Survey List ------------------------------------------------------------------------- */

        $scope.survey = {}

        dsInstitutions.getInstitutions().then(function (results) {
            $scope.institutions = results.data;;
        });


        dsTeachers.getTeachers().then(function (results) {
            $scope.teachers = results.data;
        });

        dsSubjects.getSubjects().then(function (results) {
            $scope.subjects = results.data;
        });

        $scope.addQuestion = function () {
            $(function () {
                if ($("#input-question").val().trim() != '') {
                    $("#tb_question").append('<tr><td data-pick="1"' + 'data-value="' + $("#input-question").val() + '">' + $("#input-question").val() + '</td><td><button type="button" class="btn btn-xs" ng-click="removeQuestion()"><span class="glyphicon glyphicon-trash"></span></button></td></tr>');
                    $("#input-question").val("");
                }
            });            
        }

        $("#tb_question").on('click', 'button.btn-xs', function (e) {
            e.preventDefault();
            $(this).parents('tr').remove();
        });

        $scope.saveSurvey = function () {
            var survey = {
                name: $scope.survey.Name,
                description: $scope.survey.Description,
                idInstitution: $scope.survey.institutionSelected,
                idteacher: $scope.survey.teacherSelected,
                idsubject: $scope.survey.subjectSelected,
                questions: []
            };

            $("td[data-pick='1']").each(function () {
                var value = $(this).attr("data-value");
                survey.questions.push({ Description: value });
            });

            dsSurveyAdd.createSurvey(survey).then(function (results) {
                alert("Se grabo la encuesta");
            });

        }

        


    }]);