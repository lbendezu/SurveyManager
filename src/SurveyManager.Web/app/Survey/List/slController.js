﻿angularFormsApp.controller('slController',
    ["$scope", "$location", "$window", "$routeParams", "dsTeacherInstitutionSubject", "dsSurveyList", "surveyAssessService",
    function efController($scope, $location, $window, $routeParams, dsTeacherInstitutionSubject, dsSurveyList, surveyAssessService) {

        $scope.criteria = {}

        dsTeacherInstitutionSubject.getInstitutions().then(function (results) {
            $scope.criteria.institutions = results.data;
        });

        $scope.loadTeachers = function () {
            dsTeacherInstitutionSubject.getTeachersByInstitution($scope.criteria.institutionSelected).then(function (results) {
                $scope.criteria.teachers = results.data;
            });
        }

        $scope.loadSubjects = function () {
            dsTeacherInstitutionSubject.getSubjectsByInstitutionAndTeacher($scope.criteria.institutionSelected, $scope.criteria.teacherSelected).then(function (results) {
                $scope.criteria.subjects = results.data;
            });
        }

        $scope.loadSurveys = function () {
            dsSurveyList.getAllSurveyByAllIds($scope.criteria.institutionSelected, $scope.criteria.teacherSelected, $scope.criteria.subjectSelected).then(function (results) {
                $scope.surveys = results.data;
            });
        }

        $scope.assessSurvey = function (id) {
            $location.path('/survey/assess/' + id);
        };

    }]);