﻿
angularFormsApp.factory('dsSurveyList',
    ["$http",
    function ($http) {

        var getAllSurveyByAllIds = function (idInstitution, idTeacher, idSubject) {

            return $http.get("Survey/GetAllSurveyByAllIds", { params: { IdInstitution: idInstitution, IdTeacher: idTeacher, IdSubject: idSubject } });
        };

        return {
            getAllSurveyByAllIds: getAllSurveyByAllIds
        };
    }]);