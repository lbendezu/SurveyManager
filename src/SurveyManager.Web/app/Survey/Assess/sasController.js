﻿angularFormsApp.controller('sasController',
    ["$scope", "$location", "$window", "$routeParams", "dsSurveyAssess",
    function efController($scope, $location, $window, $routeParams, dsSurveyAssess) {

        if ($routeParams.id)
            dsSurveyAssess.getSurvey($routeParams.id).then(function (results){
                $scope.survey = results.data;
            });
        else
            $scope.survey = { id: 0 };

        $scope.saveSurvey = function () {

            $scope.survey.IdSurvey = $scope.survey.Id;

            for (var i = 0; i < $scope.survey.Questions.length; i++) {
                $scope.survey.Questions[i].IdQuestion = $scope.survey.Questions[i].Id;
            }

            dsSurveyAssess.assessSurvey($scope.survey).then(function (results) {
                alert("Se grabaron sus respuestas");
                $location.path('/survey/index');
            });            
        };
        
    }]);