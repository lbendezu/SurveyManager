﻿
angularFormsApp.factory('dsSurveyAssess',
    ["$http",
    function ($http) {

        var getSurvey = function (idSurvey) {
            return $http.get("Survey/GetSurvey", { params: { id: idSurvey } });
        };

        var assessSurvey = function (survey) {
            return $http.post("Survey/Assess", survey);
        };
        
        return {
            getSurvey: getSurvey,
            assessSurvey: assessSurvey
        };
    }]);