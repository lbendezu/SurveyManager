﻿angularFormsApp.service('surveyAssessService',
    ["$http",
    function ($http) {

        var idSurvey = 0;

        var setIdSurvey = function(id) {
            idSurvey = id;
        }

        var getIdSurvey = function(){
            return idSurvey;
        }

        return {
            getIdSurvey: getIdSurvey,
            setIdSurvey: setIdSurvey
        };
    }]);