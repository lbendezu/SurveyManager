﻿angularFormsApp.controller('iaController',
    ["$scope", "$window", "$routeParams", "dsInstitutionsAdd",
    function iaController($scope, $window, $routeParams, dsInstitutionsAdd) {

        $scope.institution = {}

        $scope.saveInstitution = function () {

            $scope.$broadcast('show-errors-event');

            if ($scope.institutionForm.$invalid)
                return;

            var institution = {
                name: $scope.institution.Name
            };

            dsInstitutionsAdd.createInstitution(institution).then(function (results) {
                alert("Se grabo el instituto");
            });

        }
    }]);