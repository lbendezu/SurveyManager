﻿angularFormsApp.factory('dsInstitutionsAdd',
    ["$http",
    function ($http) {

        var createInstitution = function (institution) {
             return $http.post("Institution/Create", institution);
        };

        return {
            createInstitution: createInstitution
        };
    }]);