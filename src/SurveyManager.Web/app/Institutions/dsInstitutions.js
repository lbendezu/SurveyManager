﻿angularFormsApp.factory('dsInstitutions',
    ["$http",
    function ($http) {

        var getInstitutions = function () {

            return $http.get("Institution/GetAllInstitutions");
        };
        
        return {
            getInstitutions: getInstitutions
        };
    }]);