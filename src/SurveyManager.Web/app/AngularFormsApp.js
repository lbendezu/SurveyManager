﻿
var angularFormsApp = angular.module('angularFormsApp', ["ngRoute", "ui.bootstrap"]);

angularFormsApp.controller("HomeController",
    ["$scope", "$location",
    function ($scope, $location) {

        //efDataService.getEmployees().then(function (results) {
        //    var data = results.data;
        //});

        $scope.showCreateEmployeeForm = function () {
            $location.path('/newEmployeeForm');
        };

        $scope.showUpdateEmployeeForm = function (id) {
            $location.path('/updateEmployeeForm/' + id)
        };

        $scope.showSurveys = function () {
            $location.path('/survey/index');
        };

        $scope.addSurvey = function () {
            $location.path('/survey/add');
        };

        $scope.addTeacher = function () {
            $location.path('/teachers/add');
        };

        $scope.addInstitucion = function () {
            $location.path('/institutions/add');
        };

        $scope.addSubject = function () {
            $location.path('/subjects/add');
        };

    }]);

