﻿angularFormsApp.factory('dsSubjectsAdd',
    ["$http",
    function ($http) {

        var createSubject = function (subject) {
             return $http.post("Subject/Create", subject);
        };

        return {
            createSubject: createSubject
        };
    }]);