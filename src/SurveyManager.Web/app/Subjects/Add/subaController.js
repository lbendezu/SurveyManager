﻿angularFormsApp.controller('subaController',
    ["$scope", "$window", "$routeParams", "dsSubjectsAdd",
    function subaController($scope, $window, $routeParams, dsSubjectsAdd) {

        $scope.subject = {}

        $scope.saveSubject = function () {
            
            $scope.$broadcast('show-errors-event');

            if ($scope.subjectForm.$invalid)
                return;

            var subject = {
                name: $scope.subject.Name
            };

            dsSubjectsAdd.createSubject(subject).then(function (results) {
                alert("Se grabo el curso");
            });

        }
    }]);