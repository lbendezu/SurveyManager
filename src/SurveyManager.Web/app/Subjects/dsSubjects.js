﻿angularFormsApp.factory('dsSubjects',
    ["$http",
    function ($http) {

        var getSubjects = function () {

            return $http.get("Subject/GetAllSubjects");
        };

        return {
            getSubjects: getSubjects
        };
    }]);