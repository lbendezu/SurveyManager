﻿angularFormsApp.factory('dsTeachers',
    ["$http",
    function ($http) {

        var getTeachers = function () {

            return $http.get("Teacher/GetAllTeachers");
        };

        return {
            getTeachers: getTeachers
        };
    }]);