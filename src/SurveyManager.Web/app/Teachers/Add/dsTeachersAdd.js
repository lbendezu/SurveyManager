﻿angularFormsApp.factory('dsTeachersAdd',
    ["$http",
    function ($http) {

        var createTeacher = function (teacher) {

            return $http.post("Teacher/Create", teacher);
        };

        return {
            createTeacher: createTeacher
        };
    }]);