﻿angularFormsApp.controller('taController',
    ["$scope", "$window", "$routeParams", "dsTeachersAdd",
    function taController($scope, $window, $routeParams, dsTeachersAdd) {
        
        $scope.teacher = {}

        $scope.saveTeacher = function () {

            $scope.$broadcast('show-errors-event');

            if ($scope.teacherForm.$invalid)
                return;

            var teacher = {
                name: $scope.teacher.Name
            };

            dsTeachersAdd.createTeacher(teacher).then(function (results) {
                alert("Se grabo al profesor");
            });

        }
    }]);