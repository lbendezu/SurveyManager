﻿angularFormsApp.factory('dsTeacherInstitutionSubject',
    ["$http",
    function ($http) {

        var getInstitutions = function () {

            return $http.get("TeacherInstitutionSubject/GetInstitutions");
        };

        var getTeachersByInstitution = function (idInstitution) {

            return $http.get("TeacherInstitutionSubject/GetTeachersByInstitution", { params: { IdInstitution: idInstitution } });
        };

        var getSubjectsByInstitutionAndTeacher = function (idInstitution, idTeacher) {

            return $http.get("TeacherInstitutionSubject/GetSubjectsByInstitutionAndTeacher", { params: { IdInstitution: idInstitution, IdTeacher: idTeacher } });
        };

        return {
            getInstitutions: getInstitutions,
            getTeachersByInstitution: getTeachersByInstitution,
            getSubjectsByInstitutionAndTeacher: getSubjectsByInstitutionAndTeacher
        };

    }]);