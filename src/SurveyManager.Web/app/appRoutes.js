﻿angularFormsApp.config(["$routeProvider", "$locationProvider",
    function ($routeProvider, $locationProvider) {
        $routeProvider
            .when("/home", {
                templateUrl: "app/Home.html",
                controller: "HomeController"
            })
            .when("/newEmployeeForm", {
                templateUrl: "app/EmployeeForm/efTemplate.html",
                controller: "efController"
            })
            .when("/updateEmployeeForm/:id", {
                templateUrl: "app/EmployeeForm/efTemplate.html",
                controller: "efController"
            })
            .when("/survey/index", {
                templateUrl: "app/Survey/List/sfSurveyListTemplate.html",
                controller: "slController"
            })
            .when("/survey/add", {
                templateUrl: "app/Survey/Add/saSurveyAdd.html",
                controller: "saController"
            })
            .when("/survey/assess/:id", {
                templateUrl: "app/Survey/Assess/saSurveyAssess.html",
                controller: "sasController"
            })
            .when("/teachers/add", {
                templateUrl: "app/Teachers/Add/taTeachersAdd.html",
                controller: "taController"
            })
            .when("/institutions/add", {
                templateUrl: "app/Institutions/Add/iaInstitutionsAdd.html",
                controller: "iaController"
            })
            .when("/subjects/add", {
                templateUrl: "app/Subjects/Add/saSubjectsAdd.html",
                controller: "subaController"
            })
            .otherwise({
                redirectTo: "/home"
            });
        $locationProvider.html5Mode(true);
    }]);