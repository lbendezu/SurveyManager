﻿using AutoMapper;
using SurveyManager.Common;
using SurveyManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurveyManager.Web.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<Survey, SurveyViewModel>()
                .ForMember(dest => dest.InstitutionName,opt => opt.MapFrom(src => src.TeacherInstitutionSubject.Institution.Name))
                .ForMember(dest => dest.TeacherName,opt => opt.MapFrom(src => src.TeacherInstitutionSubject.Teacher.Name))
                .ForMember(dest => dest.SubjectName,opt => opt.MapFrom(src => src.TeacherInstitutionSubject.Subject.Name));
            Mapper.CreateMap<Question, QuestionViewModel>();
        }
    }
}